class MyCustomTag extends HTMLElement {

	constructor() {
		super();
	}

	connectedCallback() {
		this.innerHTML =
			'<img src="/images/javascript.png" alt="Javascript Logo" class="logo" />' +
			'<p>Hello <strong>'+ this.getAttribute('name') +'</strong> from your friendly Vanilla JS component.</p> ' +
			'<button type="submit" style="display:none" id="buttonVanilla" class="btn btn-secondary" onclick="dispatchHello()">Say hello</button>';
	}

	static get observedAttributes() {
		return ['name'];
	}

	attributeChangedCallback(attr, oldVal, newVal) {
		switch (attr) {
			case 'name':
				console.log('Changed name, new name is ', newVal);
				const button = document.querySelector('#buttonVanilla');
				button.style.display = '';
			default: 
		}
	}

	disconnectedCallback() {
		// called when the wc is inserted in the DOM
	}

}

if (!customElements.get('vanillajs-el')) {
	customElements.define('vanillajs-el', MyCustomTag);
}

function dispatchHello(){
	const button = document.querySelector('#buttonVanilla');
	// si composed: false (default) los consumidores no podrán escuchar el evento fuera de tu shadow root.
	button.dispatchEvent(new Event('helloEvt', {bubbles: true, composed: true}));
}