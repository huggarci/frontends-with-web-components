function tellComponents() {
    const name = document.getElementById('yourName').value;

    if(!name || name === '') {
      alert('Write your name first! :)');
      return;
    }

    const reactEl = document.getElementById('react-el-id');
    reactEl.setAttribute('name', name);
    reactEl.setAttribute('onHelloEvt', 'onHelloEvt');
    reactEl.addEventListener('onHelloEvt', (e) => helloEvent('React', name));

    const ngEl = document.getElementById('ng-el-id');
    ngEl.setAttribute('name', name);
    ngEl.addEventListener('helloEvt', (e) => helloEvent('Angular', name));

    const vanillajsEl = document.getElementById('vanillajs-el-id');
    vanillajsEl.setAttribute('name', name);
    vanillajsEl.addEventListener('helloEvt', (e) => helloEvent('VanillaJS', name));

    logMessage('You', `my name is ${name}`)

}

function helloEvent(who, toWho) {
    console.log('helloEventReceived from '+who+' to '+toWho);
    logMessage(who, 'hello '+toWho);
}

function logMessage(source, msg) {
    const msgContainer = document.getElementById('messages');
    msgContainer.innerHTML += `<p><strong>${source}</strong> said: ${msg}`;
}
