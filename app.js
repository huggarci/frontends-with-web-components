const express = require('express');
const appNg = express();
const appReact = express();
const appWrapper = express();
const appVanilla = express();

appNg.use(express.static('micro-fe-ng/dist/micro-fe-ng'));

appNg.listen(5001, () => {
  console.log('Server Ng is running at:',5001);
});


appReact.use(express.static('micro-fe-react/dist'));

appReact.listen(5002, () => {
  console.log('Server React is running at:',5002);
});

appVanilla.use(express.static('micro-fe-vanillajs'));

appVanilla.listen(5003, () => {
  console.log('Server Vanilla is running at:',5003);
});



appWrapper.use(express.static('micro-fe-wrapper'));

appWrapper.listen(5000, () => {
  console.log('Server Wrapper is running at:',5000);
});
